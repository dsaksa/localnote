import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Read from "./views/Read.vue";
import Edit from "./views/Edit.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/note/new",
      name: "create",
      component: Edit
    },
    {
      path: "/note/:slug",
      name: "read",
      component: Read
    },
    {
      path: "/note/:slug/edit",
      name: "edit",
      component: Edit
    },
    {
      path: "*",
      name: "404",
      component: Home
    }
  ]
});
