import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import VueLocalStorage from 'vue-localstorage'
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';

Vue.config.productionTip = false;
Vue.use(VueLocalStorage)

M.AutoInit();

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
